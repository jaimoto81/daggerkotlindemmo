package com.jaimoto.daggerdemo.login

data class User(var firstName: String, var lastName: String){
    var id: Int = 0;
}