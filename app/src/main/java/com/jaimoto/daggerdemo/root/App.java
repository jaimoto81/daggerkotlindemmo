package com.jaimoto.daggerdemo.root;


import android.app.Application;
import android.util.Log;

import com.jaimoto.daggerdemo.login.LoginModule;
import com.medallia.digital.mobilesdk.MDExternalError;
import com.medallia.digital.mobilesdk.MDResultCallback;
import com.medallia.digital.mobilesdk.MedalliaDigital;


//logic of the app
public class App extends Application{

    private ApplicationComponent component;

    @Override
    public void onCreate() {
         super.onCreate();

         component = DaggerApplicationComponent.builder().
                applicationModule(new ApplicationModule(this))
                 .build();



        MedalliaDigital.init(this, "eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhcGlUb2tlblYyIiwiYXV0aFVybCI6Imh0dHBzOi8vbW9iaWxlc2RrLXVzLmthbXB5bGUuY29tL2FwaS92MS9hY2Nlc3NUb2tlbiIsImNyZWF0ZVRpbWUiOjE1NDQ0Nzk1NjIxNjAsImFwaVRva2VuVjJWZXJzaW9uIjoyLCJwcm9wZXJ0eUlkIjo0NTM2MDd9.phUM6cqyXgekZaHIrgv_yNQWr1XFLGwlNZVlJVGinwJRG1R-Z0m03inXKZuEaeJo-mdVedJX1KVInwyrxDvMco7l7M2z0nFXL5mbf24qZveYgFPFhxlFzt3Py7Iy_rO2QYHZyZ-wSXOE2l5ugPtJXfb65ZW5aFsyKYXJkVp5pndmE4e50NyoP4nJBx8qV4kcRhWCzbWmFcgId1esaTbfJbAadmTrEB7rb3L9zDCgsXrZwbiJooYaDzx4znBh1JrMWRRdcpxOtK_5mxtJ5SPDCKfi0d-F2jnkuDdjJn_VpVnY7tMHuPG5GB-pBZAQzaFrOUdP0EXiSfPgTm3iUNV-1g",
                new MDResultCallback() {
                    @Override
                    public void onSuccess() {
                        Log.v("MEDALLIA","login ok");
                    }

                    @Override
                    public void onError(MDExternalError mdExternalError){
                        Log.v("MEDALLIA","login err "+mdExternalError.getMessage());

                    }

                });

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        MedalliaDigital.showForm ( "21545",   new MDResultCallback() {
            @Override
            public void onSuccess() {
                Log.v("MEDALLIA","form 21545 ok");
            }

            @Override
            public void onError(MDExternalError mdExternalError){
                Log.v("MEDALLIA","form  21545 err "+mdExternalError.getMessage());
            }

        });

        //Meda





    }

    public ApplicationComponent getComponent() {
        return component;
    }
}
