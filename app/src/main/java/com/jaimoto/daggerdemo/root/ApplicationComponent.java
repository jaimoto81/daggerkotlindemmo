
package com.jaimoto.daggerdemo.root;

import com.jaimoto.daggerdemo.login.LoginActivity;

import javax.inject.Singleton;

import dagger.Component;

//Activities, fragments
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent  {


    void inject(LoginActivity target);


}
