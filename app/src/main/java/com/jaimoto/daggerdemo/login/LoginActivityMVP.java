package com.jaimoto.daggerdemo.login;

public interface LoginActivityMVP {

    interface Model{

    }

    interface View{

    }

    interface Presenter{

    }

}
