package com.jaimoto.daggerdemo.root;

/**
 * first class - separate module for functionality
 * Dagger will search providers and methdos
 *
 */

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class ApplicationModule {

    private Application aplication;

    public ApplicationModule(Application aplication){
        this.aplication = aplication;
    }


    //Methods which return an Object should be annotated with Provides
    //for instance ApplicationContext

    @Provides
    @Singleton
    public Context provideContext(){
        return aplication;
    }

}
