package com.jaimoto.daggerdemo.login;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jaimoto.daggerdemo.R;
import com.jaimoto.daggerdemo.root.App;
import com.medallia.digital.mobilesdk.MDExternalError;
import com.medallia.digital.mobilesdk.MDResultCallback;
import com.medallia.digital.mobilesdk.MedalliaDigital;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    EditText firstname;
    EditText lastname;
    Button boton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inject the main to the application
        ((App) getApplication()).getComponent().inject(this);

        firstname = findViewById(R.id.edit_text_first_name);
        lastname = findViewById(R.id.edit_text_last_name);
        boton = findViewById(R.id.button);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Login Pulsado",Toast.LENGTH_SHORT);
                Log.v("MEDALLIA","Calling custom parameter");
                MedalliaDigital.disableIntercept();
                MedalliaDigital.setCustomParameter("TRANSACTION_OK",true);
            }
        });



    }
}
